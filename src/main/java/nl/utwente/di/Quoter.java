package nl.utwente.di;

public class Quoter {
    public double getFahrenheit(Double celsius) {
        return (celsius *1.8) +32;
    }
}
